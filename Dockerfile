FROM node:alpine

ENV PORT=3000

WORKDIR /app

COPY . .

RUN yarn

CMD [ "yarn","run","start"]