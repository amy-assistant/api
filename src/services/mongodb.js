const { MongoClient} = require('mongodb');

const url = `mongodb://${process.env.app}-db:27017`;

(async () => {
    try {
        const mongo = await Mongo();
        const users = await mongo.collection('users');
        const messsages = await mongo.collection({name:'test'});
        users.insertOne({ name: 'test' });
        messsages.insertOne({ message: 'helloworld' });

        console.log(await getUsers());
        console.log(await getAllMessages('test'));
    } catch (err) {
        throw err
    }
})();

async function getUsers() {
    const mongo = await Mongo();
    const users = await mongo.collection('users');
    return users.find().toArray();
}

async function getAllMessages(user) {
    const mongo = await Mongo();
    const messages = await mongo.collection(`users.${user.name}.messages`);
    return messages.find().toArray();
}

async function Mongo(){
    const mongo = await MongoClient.connect(url, { useNewUrlParser: true });
    return mongo.db(process.env.app);
}

module.exports = {
    getUsers
}
