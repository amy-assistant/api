const port = process.env.PORT || 3000;

const express = require('express');
const app = express();

//const expressWs = require('express-ws')(app);

const api = require('./routes/api');
// const user = require('./routes/user-api');

app.use('/', api);
//app.use('/user', user);

//app.use('/app', express.static('testing/client'))

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});